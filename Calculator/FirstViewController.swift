//
//  FirstViewController.swift
//  Calculator
//
//  Created by Omer Waqas Khan on 01/01/2016.
//  Copyright © 2016 Apps Bucket. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var salesPriceExcludingTax: UITextField!
    @IBOutlet weak var cost: UITextField!
    
    @IBOutlet weak var grossProfit: UILabel!
    @IBOutlet weak var grossMargin: UILabel!
    @IBOutlet weak var markup: UILabel!
    
    @IBOutlet weak var taxAmount: UITextField!
    @IBOutlet weak var taxRatePercentage: UITextField!
    
    @IBOutlet weak var taxRate: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    
    @IBOutlet weak var calculateBtn: UIButton?
    
    var salesValue:Float!
    var costValue:Float!
    
    var isKeyBoardUp = false
    
    let regex = try! NSRegularExpression(pattern: ".*[^.0-9].*", options: NSRegularExpressionOptions())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.calculateBtn?.layer.cornerRadius = 3.0
        self.calculateBtn?.layer.borderWidth = 2.0
        self.calculateBtn?.layer.borderColor = UIColor.lightBlueColor().CGColor
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        
        addDoneButtonOnKeyboard()
        
        loadTabBarIcons()
    }
    
    func loadTabBarIcons() {
        
        let tabBar = self.tabBarController?.tabBar
        let firstTab = tabBar?.items![0]
        let secondTab = tabBar?.items![1]
        firstTab?.image = UIImage(named: "inactive.png")!.imageWithRenderingMode(.AlwaysOriginal)
        firstTab?.selectedImage = UIImage(named: "active.png")!.imageWithRenderingMode(.AlwaysOriginal)
        secondTab?.image = UIImage(named: "inactive.png")!.imageWithRenderingMode(.AlwaysOriginal)
        secondTab?.selectedImage = UIImage(named: "active.png")!.imageWithRenderingMode(.AlwaysOriginal)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        UINavigationBar.appearance().barTintColor = UIColor.lightBlueColor()
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    // Calculate Gross Profit
    // Sales - Cost of Goods Sold = Gross Profit
    
    func calculateGrossProfit() {
        
        self.salesValue = Float(self.salesPriceExcludingTax.text!)
        
        self.costValue = Float(self.cost.text!)
        
        let gp = salesValue - costValue
        
        self.grossProfit.text = String(gp)
        
        NSUserDefaults.standardUserDefaults().setFloat(gp, forKey: "gp")
        
        calculateGrossProfitMargin(gp)
    }
    
    // Calculate Gross Profit Margin
    // Gross Profit / Sales = Gross Profit Margin
    
    func calculateGrossProfitMargin(gp:Float) {
        
        let gpMargin = (gp/self.salesValue) * 100
        
        self.grossMargin.text = String("\(gpMargin.string(2))%")
        
        calculateMarkupPercentage(gp)
    }
    
    // Calculate Markup Percentage
    // ((Selling Price - Cost to Produce) / Cost to Produce) * 100 = Markup Percentage
    
    func calculateMarkupPercentage(gp: Float) {
        
        let markup = (gp/self.costValue) * 100
        
        self.markup.text = String("\(markup.string(2))%")
    }
    
    // Calculate Tax Rate/Percentage
    // Tax Rate = Tax / Sales Price x 100
    
    func calculateTaxRate(isRate: Bool, value: Float) {
        
        var taxAmountValue: Float!
        
        if isRate {
            
            // User entered tax percentage - calculate amount of tax
            
            self.taxLabel.text = "Tax Amount:"
            
            let tax = Float(self.taxRatePercentage.text!)!
            
            let taxCalculated = self.salesValue * (tax / 100)
            
            self.taxRate.text = String("\(taxCalculated.string(2))")
            
            taxAmountValue = taxCalculated
        }
        else {
            
            // User entered amout - calculate tax percentage
            
            self.taxLabel.text = "Tax Rate:"
            
            let tax = Float(self.taxAmount.text!)!
            
            let taxCalculated = (tax/self.salesValue) * 100
            
            self.taxRate.text = String("\(taxCalculated.string(2))%")
            
            taxAmountValue = tax
        }
        
        NSUserDefaults.standardUserDefaults().setFloat(taxAmountValue, forKey: "tax")
    }
    
    // UITextField Delegates
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField == self.taxAmount || textField == self.taxRatePercentage {
            keyboardWillShow()
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if textField == self.salesPriceExcludingTax {
            
            if self.regex.firstMatchInString(self.salesPriceExcludingTax.text!, options: NSMatchingOptions(), range:NSMakeRange(0, self.salesPriceExcludingTax.text!.characters.count)) != nil {
                
                self.salesPriceExcludingTax.text = ""
                
                alertUser("Please enter a valid value!")
            }
            else {
                
                if !self.salesPriceExcludingTax.text!.isEmpty {
                    NSUserDefaults.standardUserDefaults().setFloat(Float(self.salesPriceExcludingTax.text!)!, forKey: "sales")
                    NSUserDefaults.standardUserDefaults().synchronize()
                }
                if !self.cost.text!.isEmpty {
                    
                    calculateGrossProfit()
                }
            }
        }
        
        if textField == self.cost {
            
            if self.regex.firstMatchInString(self.cost.text!, options: NSMatchingOptions(), range:NSMakeRange(0, self.cost.text!.characters.count)) != nil {
                
                alertUser("Please enter a valid value!")
                
                self.cost.text = ""
            }
            else {
                if !self.salesPriceExcludingTax.text!.isEmpty {
                    calculateGrossProfit()
                }
            }
        }
        
        if textField == self.taxAmount || textField == self.taxRatePercentage {
            keyboardWillHide()
            
            if !textField.text!.isEmpty {
                
                if self.regex.firstMatchInString(self.taxAmount.text!, options: NSMatchingOptions(), range:NSMakeRange(0, self.taxAmount.text!.characters.count)) != nil {
                    
                    self.taxAmount.text = ""
                    
                    alertUser("Please enter a valid value!")
                }
                else if self.regex.firstMatchInString(self.taxRatePercentage.text!, options: NSMatchingOptions(), range:NSMakeRange(0, self.taxRatePercentage.text!.characters.count)) != nil {
                    
                    self.taxRatePercentage.text = ""
                    
                    alertUser("Please enter a valid value!")
                }
                else {
                    
                    self.salesValue = Float(self.salesPriceExcludingTax.text!)
                    self.costValue = Float(self.cost.text!)
                    
                    if self.salesValue != nil && self.costValue != nil {
                        
                        var taxValue:Float!
                        
                        calculateGrossProfit()
                        
                        if textField == self.taxAmount {
                            
                            self.taxAmount.enabled = true
                            self.taxRatePercentage.enabled = false
                            
                            taxValue = Float(self.taxAmount.text!)
                            
                            calculateTaxRate(false, value: taxValue)
                        }
                        if textField == self.taxRatePercentage {
                            
                            self.taxAmount.enabled = false
                            self.taxRatePercentage.enabled = true
                            
                            taxValue = Float(self.taxRatePercentage.text!)
                            
                            calculateTaxRate(true, value: taxValue)
                        }
                    }
                    else {
                        
                        if self.salesValue == nil {
                            alertUser("Please enter Sales Price (Ex.Tax) value")
                        }
                        if self.costValue == nil {
                            alertUser("Please enter Cost value")
                        }
                    }
                }
            }
            else {
                
                self.salesValue = Float(self.salesPriceExcludingTax.text!)
                self.costValue = Float(self.cost.text!)
                
                if self.salesValue != nil && self.costValue != nil {
                    
                    calculateGrossProfit()
                }
            }
        }
        else {
            
            print("issue")
        }
    }
//    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
//        print("TextField should begin editing method called")
//        return true;
//    }
//    func textFieldShouldClear(textField: UITextField) -> Bool {
//        print("TextField should clear method called")
//        return true;
//    }
//    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
//        print("TextField should snd editing method called")
//        return true;
//    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        return true;
    }
//    func textFieldShouldReturn(textField: UITextField) -> Bool {
//        print("TextField should return method called")
//        textField.resignFirstResponder();
//        return true;
//    }
    
    @IBAction func resetValues(sender: UIBarButtonItem) {
        
        self.taxRatePercentage.enabled = true
        self.taxAmount.enabled = true
        
        self.taxRatePercentage.text = ""
        self.taxAmount.text = ""
        
        self.salesPriceExcludingTax.text = ""
        self.cost.text = ""
        
        self.salesValue = nil
        self.costValue = nil
        
        self.grossProfit.text = "0.00"
        self.grossMargin.text = "0.00%"
        self.markup.text = "0.00%"
        self.taxRate.text = "0.00%"
        
        NSUserDefaults.standardUserDefaults().removeObjectForKey("gp")
        NSUserDefaults.standardUserDefaults().synchronize()
        NSUserDefaults.standardUserDefaults().removeObjectForKey("tax")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    @IBAction func calculateAction(sender: UIButton) {
        
//        var characterSet:NSCharacterSet = NSCharacterSet(charactersInString: "0123456789")
//        if (self.salesPriceExcludingTax.text!.rangeOfCharacterFromSet(characterSet.invertedSet).location == NSNotFound){
//            print("No special characters")
//        }
        
        if self.regex.firstMatchInString(self.salesPriceExcludingTax.text!, options: NSMatchingOptions(), range:NSMakeRange(0, self.salesPriceExcludingTax.text!.characters.count)) != nil {
            
            alertUser("Please enter a valid value!")
        }
        else if self.regex.firstMatchInString(self.cost.text!, options: NSMatchingOptions(), range:NSMakeRange(0, self.cost.text!.characters.count)) != nil {
            
            alertUser("Please enter a valid value!")
        }
        else {
            
            self.salesValue = Float(self.salesPriceExcludingTax.text!)
            
            self.costValue = Float(self.cost.text!)
            
            if self.salesValue != nil && self.costValue != nil {
                
                calculateGrossProfit()
            }
            else {
                
                if self.salesValue == nil {
                    alertUser("Please enter Sales Price (Ex.Tax) value")
                }
                if self.costValue == nil {
                    alertUser("Please enter Cost value")
                }
            }
        }
    }
    
    /** Aler User **/
    func alertUser(alertMessage: String) {
        
        //Create the AlertController
        let actionSheetController: UIAlertController = UIAlertController(title: "Problem", message: alertMessage, preferredStyle: .Alert)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .Cancel) { action -> Void in
            //Do some stuff
        }
        actionSheetController.addAction(cancelAction)
        
        //Present the AlertController
        self.presentViewController(actionSheetController, animated: true, completion: nil)
    }
    
    /** Move view up **/
    func keyboardWillShow() {
        
        if (!isKeyBoardUp) {
            isKeyBoardUp = true
            
            self.view.frame.origin.y -= 150
        }
    }
    /** Move view back to its normal location **/
    func keyboardWillHide() {
        
        if (isKeyBoardUp) {
            isKeyBoardUp = false
            self.view.frame.origin.y += 150
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, 320, 50))
        doneToolbar.barStyle = UIBarStyle.BlackTranslucent
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: Selector("doneButtonAction"))
        done.tintColor = UIColor.whiteColor()
        
        var btns = [AnyObject]()
        btns.append(flexSpace)
        btns.append(done)
        
        doneToolbar.items = btns as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.salesPriceExcludingTax.inputAccessoryView = doneToolbar
        self.cost.inputAccessoryView = doneToolbar
        self.taxAmount.inputAccessoryView = doneToolbar
        self.taxRatePercentage.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction()
    {
        dismissKeyboard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

