//
//  Float+DecimalPrecision.swift
//  Calculator
//
//  Created by Omer Waqas Khan on 02/01/2016.
//  Copyright © 2016 Apps Bucket. All rights reserved.
//

import Foundation

extension Float {
    
    func string(fractionDigits:Int) -> String {
        
        let formatter = NSNumberFormatter()
        
        formatter.minimumFractionDigits = fractionDigits
        formatter.maximumFractionDigits = fractionDigits
        
        return formatter.stringFromNumber(self) ?? "\(self)"
    }
}
