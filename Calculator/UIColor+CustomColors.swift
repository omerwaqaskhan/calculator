//
//  UIColor+CustomColors.swift
//  Mood Matcher
//
//  Created by Omer Waqas Khan on 23/10/2015.
//  Copyright © 2015 Apps Bucket. All rights reserved.
//

import UIKit

extension UIColor {
    
    class func lightBlueColor() -> UIColor {
        return UIColor(red: 0.0/255.0, green: 127.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    }
}
