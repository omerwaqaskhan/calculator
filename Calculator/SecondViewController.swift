//
//  SecondViewController.swift
//  Calculator
//
//  Created by Omer Waqas Khan on 01/01/2016.
//  Copyright © 2016 Apps Bucket. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var calculatedGP: UILabel!
    @IBOutlet weak var expenses: UITextField!
    @IBOutlet weak var tax: UILabel!
    
    @IBOutlet weak var netProfit: UILabel!
    @IBOutlet weak var netProfitMargin: UILabel!
    
    var iMinSessions = 3
    var iTryAgainSessions = 6
    
    let regex = try! NSRegularExpression(pattern: ".*[^.0-9].*", options: NSRegularExpressionOptions())
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
//        rateMe()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        
        addDoneButtonOnKeyboard()
    }
    
    override func viewWillAppear(animated: Bool) {
        
        let gp = NSUserDefaults.standardUserDefaults().floatForKey("gp")
        
        if gp == 0 {
            
            self.calculatedGP.text = "0.00"
            self.expenses.placeholder = "Expenses"
            self.tax.text = "0.00"
            self.netProfit.text = "0.00"
            self.netProfitMargin.text = "0.00%"
            
            alertUser("Please calculate Gross Profit for Net Profit")
        }
        else {
            
            calculateNetProfits(gp)
        }
    }
    
//    func rateMe() {
//        
//        let neverRate = NSUserDefaults.standardUserDefaults().boolForKey("neverRate")
//        var numLaunches = NSUserDefaults.standardUserDefaults().integerForKey("numLaunches") + 1
//        
//        if (!neverRate && (numLaunches == iMinSessions || numLaunches >= (iMinSessions + iTryAgainSessions + 1)))
//        {
//            showRateMe()
//            numLaunches = iMinSessions + 1
//        }
//        NSUserDefaults.standardUserDefaults().setInteger(numLaunches, forKey: "numLaunches")
//    }
//    
//    func showRateMe() {
//        let alert = UIAlertController(title: "Rate Us", message: "Thanks for using Margin Cal. Your feedback and suggestions will help us making this app better.", preferredStyle: UIAlertControllerStyle.Alert)
//        
//        alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.Default, handler: { alertAction in
//            UIApplication.sharedApplication().openURL(NSURL(string : "itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=1071764060")!)
//            alert.dismissViewControllerAnimated(true, completion: nil)
//        }))
//        alert.addAction(UIAlertAction(title: "No Thanks", style: UIAlertActionStyle.Default, handler: { alertAction in
//            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "neverRate")
//            alert.dismissViewControllerAnimated(true, completion: nil)
//        }))
//        alert.addAction(UIAlertAction(title: "Reming Me Later", style: UIAlertActionStyle.Default, handler: { alertAction in
//            alert.dismissViewControllerAnimated(true, completion: nil)
//        }))
//        self.presentViewController(alert, animated: true, completion: nil)
//    }
    
    func calculateNetProfits(gp: Float) {
        
        self.calculatedGP.text = String("\(gp.string(2))")
        let taxAmount = NSUserDefaults.standardUserDefaults().floatForKey("tax")
        self.tax.text = String("\(taxAmount.string(2))")
        var nProfit = gp - NSUserDefaults.standardUserDefaults().floatForKey("tax")
        
        if !self.expenses.text!.isEmpty {
            
            nProfit = nProfit - Float(self.expenses.text!)!
        }
        
        self.netProfit.text = String("\(nProfit.string(2))")
        
        let npMargin = (nProfit/NSUserDefaults.standardUserDefaults().floatForKey("sales")) * 100
        self.netProfitMargin.text = String("\(npMargin.string(2))%")
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    func textFieldDidEndEditing(textField: UITextField) {
        
        if self.regex.firstMatchInString(self.expenses.text!, options: NSMatchingOptions(), range:NSMakeRange(0, self.expenses.text!.characters.count)) != nil {
            
            self.expenses.text = ""
            alertUser("Please enter a valid value!")
        }
        else {
            if !self.expenses.text!.isEmpty {
                
                let gp = NSUserDefaults.standardUserDefaults().floatForKey("gp")
                
                if gp == 0 {
                    
                    self.expenses.text = ""
                    alertUser("Please calculate Gross Profit for Net Profit")
                }
                else {
                    
                    let expenses = Float(self.expenses.text!)
                    let grossProfit = Float(NSUserDefaults.standardUserDefaults().floatForKey("gp"))
                    let nProfit = grossProfit - NSUserDefaults.standardUserDefaults().floatForKey("tax") - expenses!
                    
                    self.netProfit.text = String("\(nProfit.string(2))")
                    
                    let npMargin = (nProfit / NSUserDefaults.standardUserDefaults().floatForKey("sales")) * 100
                    self.netProfitMargin.text = String("\(npMargin.string(2))%")
                }
            }
            else {
                calculateNetProfits(Float(NSUserDefaults.standardUserDefaults().floatForKey("gp")))
            }
        }
    }
    
    /** Aler User **/
    func alertUser(alertMessage: String) {
        
        //Create the AlertController
        let actionSheetController: UIAlertController = UIAlertController(title: "Problem", message: alertMessage, preferredStyle: .Alert)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .Cancel) { action -> Void in
            //Do some stuff
        }
        actionSheetController.addAction(cancelAction)
        
        //Present the AlertController
        self.presentViewController(actionSheetController, animated: true, completion: nil)
    }
    
    func addDoneButtonOnKeyboard() {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, 320, 50))
        doneToolbar.barStyle = UIBarStyle.BlackTranslucent
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: Selector("doneButtonAction"))
        done.tintColor = UIColor.whiteColor()
        
        var btns = [AnyObject]()
        btns.append(flexSpace)
        btns.append(done)
        
        doneToolbar.items = btns as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.expenses.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        dismissKeyboard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

